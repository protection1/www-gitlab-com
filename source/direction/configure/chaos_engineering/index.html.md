---
layout: markdown_page
title: "Category Direction - Chaos Engineering"
---

- TOC
{:toc}

## Chaos Engineering

Chaos engineering is the discipline of experimenting on a software system in production in order to build confidence in the system’s capability to withstand turbulent and unexpected conditions.

We want to safely allow operators to run downtime scenarios in pre-prod & prod   environments randomly. Starting with the min unit (pod) all the way to the max unit (cluster).
Once operators have built/configured a good fail-over plan, allow then to run downtime scenarios in production environments.
Provide relevant metrics alongside incidents.


- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=chaos%20engineering)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/381) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

[Add Litmus Chaos to GitLab-managed apps](https://gitlab.com/gitlab-org/gitlab/issues/36789)

We want to provide our Kubernetes users an easy way to get started with Chaos Engineering. Litmus is a toolset to do cloud-native chaos engineering. Litmus provides tools to orchestrate chaos on Kubernetes to help SREs find weaknesses in their deployments. SREs use Litmus to run chaos experiments initially in the staging environment and eventually in production to find bugs, vulnerabilities. Fixing the weaknesses leads to increased resilience of the system.

After users have the ability to easily install Litmus, we plan to incorporate these capabilites into Auto DevOps.

## Competitive landscape

### Gremlin
Gremlin provides a framework to safely, securely, and easily simulate real outages with an ever-growing library of attacks.

### ChaosToolkit
Chaos Toolkit is a project whose mission is to provide a free, open and community-driven toolkit and API to all the various forms of chaos engineering tools that the community needs.

## Analyst landscape

TBD

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

[Gitlab Chaos Templates for Kubernetes-based CI/CD](https://gitlab.com/gitlab-org/gitlab/issues/11353)

## Top internal customer issue(s)

[Add Litmus Chaos to GitLab-managed apps](https://gitlab.com/gitlab-org/gitlab/issues/36789)

## Top Vision Item(s) 

[Add Litmus Chaos to GitLab-managed apps](https://gitlab.com/gitlab-org/gitlab/issues/36789)
