---
layout: handbook-page-toc
title: "Business Continuity Plan"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Context
GitLab, by its remote-only nature, is not easily affected by typical causes of business disruption, such as local failures of equipment, power supplies, telecommunications, social unrest, terrorist attacks, fire, or natural disasters. 

## Business Continuity Plan for Remote Workers 
In case of an all-remote company like GitLab, it is sufficient to have simple contingency plans in the form of service-level agreements with companies that host our data and services. The advantage of an all-remote workforce like GitLab is that if there are clusters of people or systems that are unavailable, the rest of the company will continue to operate normally.

The exception to this would be a scenario of a single point of failure, (for example, if one of the Engineering heads who should sign off on triggering the plan is unavailable due to a disaster). In this case we would need an alternate plan in place that covers how to get in contact with the person or people affected by the disaster and trigger this business continuity plan.

## Recovery Time Objective (RTO) and Recovery Point Objective (RPO)
RTO and RPO are two of the most important parameters of a Business Continuity Plan. These are objectives to guide GitLab Infrastructure team in choosing the optimal data backup plan. The RTO/RPO, provides the basis for identifying and analyzing viable strategies for inclusion in the business continuity plan. Viable strategy options include any which would enable resumption of a business process in a time frame within the RPO/RTO.

### What is a Recovery Point Objective? 
Recovery Point Objective (RPO) is the interval of time that might pass during a disruption before the quantity of data lost during that period exceeds the Business Continuity Plan’s maximum allowable threshold. 

### What is a Recovery Time Objective? 
The Recovery Time Objective (RTO) is the duration of time a service level or business process must be restored after a disaster, in order to avoid risks associated with a break in continuity. 

## What triggers the Business Continuity Plan
For a business continuity plan to be effective, it needs to be triggered as soon as possible; too early or late can reduce its efficacy. Key decision points to consider when a BCP has to be triggered or invoked are given below:
* When an incident turns into an event like a disaster, breach, or something which classifies is as a [Severity 1](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues)
* When the estimated time of resolution for a potential breach is greater than the normal estimated time for regular [security incidents](/handbook/engineering/security/sec-incident-response.html)
* When the recovery of an incident is uncertain, a decision must be made to invoke the business continuity plan if the disruption cannot be resolved within the specified [incident recovery timelines](/handbook/engineering/security/sec-incident-response.html)
* When resolution of an incident with critical customers, depending on their service-level agreements is delayed, then the BC plan must be triggered


## Data Continuity System
This section provides details about the [Production environment](https://about.gitlab.com/handbook/engineering/security/sec-controls.html#what-is-considered-production) that must be available for GitLab.com to run effectively: 
 
GitLab.com is hosted on Google cloud platform, customers.gitlab.com is in Azure and license.gitlab.com is in AWS. Since Customers and Licenses are hosted on different providers, they are unlikely to be unavailable when/if GitLab.com is down; the converse of this can also be true.


**P1: Outage would have immediate impact on GitLab customer/user operations**

1. Disruption of service of Google Cloud Platform, specifically the region in which GitLab.com and dev.gitlab.org are hosted.
   - Effect: a loss or degradation of service, of the Google Cloud Platform means that GitLab.com is not available. This affects anyone who uses GitLab.com to host their repositories and may prevent GitLab team members from completing their work. GitLab.com is also the primary server where GitLab CE and EE source code and packages are hosted.
   - Solution(s): There are many other servers across the globe where GitLab CE is readily available.
   - Effect: Security releases are developed and staged on dev.gitlab.org before being brought to production on GitLab.com; these may be lost or unavailable for the duration of the disruption.
   - Solution(s): Depending on the duration and nature of the disruption, the solution is to wait for service to be restored (minimal duration), or build a new staging server. Using VM snapshots, recovery from backup is relatively quick.

1. Unavailability of support staff in case of a customer emergency.
   - Effect: emergency response times are greater than intended.
   - Solution(s): The team is distributed geographically (except during team get-togethers). Customer emergencies are handled by _any_ person who is in the [on-call rotation](/handbook/on-call). The on-call load is distributed at many levels, service engineers, production engineers, and even developers can be summoned when we have an outage or a customer incident. Emergencies also trigger automatic notifications on our internal chat system, alerting the entire company. There is also an ongoing effort to publish our [runbooks](https://gitlab.com/gitlab-com/runbooks), explaining how we manage our infrastructure and how we deal with outage cases.

1. Disruption of service of ZenDesk.
   - Effect: support workflows are disrupted. New tickets cannot be created, existing tickets cannot be responded to.
   - Solution(s): For the duration of the outage (if more than e.g. 4 hours) temporarily re-route incoming support requests to individual email accounts of members of the support team. Customers with premium support also have access to a direct chat channel.

**P2: Outage would have immediate impact on GitLab ability to continue business**
Malicious Software attack and hacking or other Internet attacks.
   - Effect: depends on attack.
   - Solution(s): We log and track any access that happens on any server in the fleet using logstash/kibana at log.gitlab.net.

**P3: Outage greater than 72 hours would have impact on GitLab ability to continue to do business**
Disruption of service from Salesforce.com, Zuora, NetSuite, G-suite
   - No failover plan currently.

**P4: Non critical system**
Disruption of service from TripActions or internal chat tool (Slack).
   - When TrpActions is down, team members can use their own travel booking tool and expense it to GitLab with reason for exception
   - When Slack is down, team members can use Google hangouts. 


## Communication Plan and Role Assignments
When it comes to a disaster, communication is of the essence. A plan is essential because it puts all team-members on the same page and clearly outlines all communication. Documents should all have updated team-member contact information and team-members should understand exactly what their role is, in the days following the triggering of the BC plan. Assignments like setting up workstations, assessing damage, redirecting phones and other tasks will need assignments if you don’t have some sort of technical resource to help you sort through everything.

Each GitLab team should be trained and ready to deploy in the event of a disruptive situation requiring plan activation. The plan of action steps, procedures, and guidelines will be documented in their team runbooks page (currently under development) and should be available offline. This should have detailed steps on recovery capabilities, and instructions on how to return the system to normal operations.

More details on this will be covered in the `BC plan - roles & responsibilities section which is in development`.  


## Backup check
[Make sure that backups are performed daily](/handbook/engineering/security/guidance/BU.1.01_backup_configuration.html), and include running an additional full local backup on all servers and data in the Business Continuity preparation plan. Run them as far in advance as possible tp ensure that they’re backed up to a location that will not be impacted by the disaster. [Alternate storage provisioning](/handbook/engineering/security/guidance/BU.1.03_alternate_storage.html).


## Distribute and Verify the Plan / Approval from Senior management
* Gitlab documentation related to all procedure and guidelines detailing the Business Continuity and Disaster Recovery must be reviewed, updated, and formally approved by [GitLab leadership](/handbook/leadership/)
* After developing basic guidelines, this plan will be distributed as a work in progress to the core team
* The core team will review to verify that all technical details are covered and deficiencies exist
* The core team will review and add guidelines so that all related [DRIs](/handbook/people-group/directly-responsible-individuals/) are clear on what is required in all situations
* GitLab team members must be asked to confirm their current details, such as phone numbers and emergency contacts on an annual basis
* Managers or team leads will share the relevant parts of the plan to all Gitlab team mebers based on their role and department, and verify that they know what to do in the event of an emergency

## Vendor communication and service restoration plan
A plan cannot be successful without restoring customer confidence. As a final step, ensure that there is a detailed vendor communication plan as part of the Business continuity preparation plan. This plan will check for all the systems and services to ensure normal operations have resumed as intended once the damage is repaired in the area. Also include the secion to check with the main service providers on restoration and access.
