require 'date'

# BambooHR::Employee data with extra manager_id attribute
EMPLOYEE_DATA = [
  {
    "employeeNumber" => 101,
    "preferredName" => nil,
    "firstName" => "Chris",
    "lastName" => "GitLab",
    "department" => "Product",
    "jobTitle" => "CEO",
    "hireDate" => Date.new(2015, 1, 1),
    "country" => "United States",
    "manager_id" => nil
  },
  {
    "employeeNumber" => 102,
    "preferredName" => nil,
    "firstName" => "Kim",
    "lastName" => "Smith",
    "department" => "Development",
    "jobTitle" => "Engineering Manager",
    "hireDate" => Date.new(2016, 1, 1),
    "country" => "United States",
    "manager_id" => 101
  }
].freeze

# team.yml-like entries with limited data
TEAM_ENTRIES = [
  {
    "slug" => "chris",
    "type" => "person",
    "name" => "Chris GitLab",
    "role" => "Chief Executive Officer",
    "start_date" => Date.new(2015, 1, 1),
    "reports_to" => "a-board-member"
  },
  {
    "slug" => "kim",
    "type" => "person",
    "name" => "Kim Smith",
    "role" => "Frontend Engineer",
    "start_date" => Date.new(2019, 1, 1),
    "reports_to" => "chris"
  }
].freeze

describe BambooHR::Matcher do
  let(:employees) { EMPLOYEE_DATA.map { |entry| BambooHR::Employee.new(entry, entry['manager_id']) } }

  describe '.match' do
    it 'returns the expected number of matches and misses' do
      matches, misses = described_class.match(employees, TEAM_ENTRIES)

      expect(matches.length).to eq 1
      expect(misses.length).to eq 1
    end

    it 'returns the expected matches and misses' do
      matches, misses = described_class.match(employees, TEAM_ENTRIES)
      match_bamboo = matches[0][0]
      match_team = matches[0][1]
      miss = misses[0]

      expect(match_bamboo.name).to eq 'Chris GitLab'
      expect(match_team['name']).to eq 'Chris GitLab'
      expect(miss.name).to eq 'Kim Smith'
    end
  end
end
